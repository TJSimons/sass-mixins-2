module.exports = function(grunt){
    grunt.loadNpmTasks("grunt-contrib-concat");

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            'dist/_mixins.scss': ['src/**/*.scss']
        }
    });

    grunt.registerTask('default', ['concat']);
};
